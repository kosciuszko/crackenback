ifndef_any_of = $(filter undefined,$(foreach v,$(1),$(origin $(v))))
ifdef_any_of = $(filter-out undefined,$(foreach v,$(1),$(origin $(v))))

.DEFAULT_GOAL := check-aws-access-token

check-aws-access-token:
ifndef AWS_ACCESS_KEY_ID
ifndef AWS_PROFILE
  echo succeeded
else
  $(error AWS_ACCESS_KEY_ID and AWS_PROFILE undefined)
endif
endif

check-env-vars: check-aws-access-token
ifeq ($MAKECMDGOALS, deploy)
ifeq ($(call ifndef_any_of,MAKE_APP MAKE_ENV MAKE_REPO MAKE_ENV_ACCOUNT_ID),)
  echo succeeded
else
  $(error MAKE_APP MAKE_ENV MAKE_REPO or MAKE_ENV_ACCOUNT_ID undefined)
endif
endif

validate: check-env-vars
	aws cloudformation validate-template \
	--template-body file://cicd/stack.yml \
	--region ap-southeast-2
	aws cloudformation validate-template \
	--template-body file://app/infra/stack.yml \
	--region ap-southeast-2
	aws cloudformation validate-template \
	--template-body file://app/infra/params/stack.yml \
	--region ap-southeast-2

deploy: validate
	aws cloudformation deploy \
	--stack-name $MAKE_APP-$MAKE_ENV-stack
	--template-file cicd/stack.yaml \
	--parameter-overrides \
		app=$MAKE_app \
		env=$MAKE_ENV \
		repo=$MAKE_REPO \
		envAccountId=$MAKE_ENV_ACCOUNT_ID \
	--capabilities CAPABILITY_NAMED_IAM \
	--region ap-southeast-2 \
	--no-fail-on-empty-changeset

_events: check-aws-access-token
	aws cloudformation describe-stack-events \
	--stack-name $MAKE_APP-$MAKE_ENV-stack

_output: check-aws-access-token
	aws cloudformation describe-stacks \
	--stack-name $MAKE_APP-$MAKE_ENV-stack

