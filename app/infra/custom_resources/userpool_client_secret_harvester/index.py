import os

import boto3
from bluecow import CustomResource

class SecretHarvester(CustomResource):

    def create(self):
        """get the client secret of the userpool client"""
        cognito = boto3.client('cognito-idp')
        c = {
            'UserPoolId': os.environ['USERPOOL'],
            'ClientId': os.environ['CLIENT']
        }
        resp = cognito.describe_user_pool_client(**c)
        return {
            'secret': resp['UserPoolClient']['ClientSecret']
        }

    def update(self):
        """do nothing"""
        pass

    def delete(self):
        """do nothing"""
        pass

def handler(event, context):
    """lambda handler"""
    sh = SecretHarvester(event, context)
    sh.execute()
