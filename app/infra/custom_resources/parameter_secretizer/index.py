import os

import boto3
from bluecow import CustomResource

SSM = boto3.client('ssm')

class ParamSecretizer(CustomResource):

    def create(self):
        """update the param type"""
        p = {
            'Name': os.environ['PARAM_NAME'],
            'Value': '',
            'Type': 'SecureString',
            'Overwrite': True
        }
        resp = SSM.put_parameter(**p)

    def update(self):
        """do nothing"""
        pass

    def delete(self):
        """do nothing"""
        pass

def handler(event, context):
    """lambda handler"""
    ps = ParamSecretizer(event, context)
    ps.execute()
