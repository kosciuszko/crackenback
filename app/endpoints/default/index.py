import base64, json, os

def handler(event, context):
    """default handler - authenticated 404"""
    headers = event['multiValueHeaders']
    access_token = headers['x-amzn-oidc-accesstoken']
    payload = access_token[0].split('.')[1]
    decoded_payload = base64.b64decode(f'{payload}===')
    username = json.loads(decoded_payload)['username']
    return {'statusCode': 404,
            'statusDescription': '404 Resource Not Found',
            'isBase64Encoded': False,
            'multiValueHeaders': {'Content-Type': ['text/html']},
            'body': f'''<!DOCTYPE html>
                        <html>
                            <head>
                                <title>{os.environ['app_name']}</title>
                            </head>
                            <body>
                                <div>logged in as {username}</div>
                                <div>logout link</div>
                                <div>404 Resource Not Found</div>
                            </body>
                        </html>'''}
